// SPDX-License-Identifier: MIT

pragma solidity 0.8.13;

struct ChestInfo {
    uint256 id;
    uint256 fee;
    string name;
    bool initialized;
}
